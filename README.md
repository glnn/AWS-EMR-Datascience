# Terraform AWS EMR & RStudio Server

This will deploy an EMR cluster and install RStudio Server on the head node.

# Preparations

## Prepare SSH key

First of all you need to create an SSH key pair for securely accessing the cluster.

    ssh-keygen -t rsa -C "EMR Access Key" -f deployer-key
    cat deployer-key.pub

## Create AWS Configuration

You need to copy the `aws-config.tf.template` file to `aws-config.tf` and modify
it so that it contains your AWS credentials and the desired AWS region and
availability zone. You can also specify the SSH Key.

## Modify General Configuration

Now that you have everything together, you also might want to adjust some
settings in `main.tf`. Per default one EMR clusters will be created, consisting of two nodes (one master and one worker).

# Running the Clusters

## Start Cluster

    terraform get
    terraform apply

## Destroy Cluster

    terraform destroy

## Connect to Cluster

    ssh -i deployer-key hadoop@ec2-35-176-55-210.eu-west-2.compute.amazonaws.com
